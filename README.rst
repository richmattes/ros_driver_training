===================
ROS Driver Training
===================

Overview
========
The ROS Driver Training project provides a way to practice, learn, and become comfortable with the process of writing ROS drivers for different types of devices.  To accomplish that end, this package provides a series of lessons targeting ROS driver node development, as well as supplimentary software that allows users to test and debug their code.

How it works
============
The ROS driver training repository comes with a set of executable programs that mimic hardware devices talking over a variety of protocols. Users write ROS nodes that interpret data coming from these programs as though they were coming from a hardware device.

Device Communication
--------------------
The ROS driver training program contains lessons with programs that communicate over the following common transport mechanisms:

* TCP 
* UDP
* Serial (via Arduino sketches)

The programs in the lessons use a vareiety of protocols to transmit data over the above interfaces, including:

* Packetized text strings
* Delimited text string
* Message-based binary protocols
* Stream-based binary protocols
* Active protocols (e.g. the sensor requires commands before sending feedback)
* Configurable Sensors (e.g. the sensor can operate in several different modes based on user commands)

Driver Development
------------------
In addition to communication and data parsing, the program includes tips on best practices for writing ROS drivers, both in C++ and Python. These best practices should guide you in writing driver nodes as well as non-driver ROS nodes.

Feedback
========
Please submit questions, comments, improvements and bug reports to the project issue tracker.
