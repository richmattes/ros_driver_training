cmake_minimum_required(VERSION 2.8.12)
project(ros_driver_training)

# Set the project version using semantic versioning (http://semver.org)
set(PROJECT_VERSION_MAJOR 0)
set(PROJECT_VERSION_MINOR 0)
set(PROJECT_VERSION_PATCH 1)

set(PROJECT_VERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")

find_package(catkin REQUIRED)
catkin_package()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include(GNUInstallDirs)

# Find the project dependencies (Flexiport)
find_package(Flexiport REQUIRED)

# Add source directory
add_subdirectory(src)

install(FILES package.xml DESTINATION ${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME})
