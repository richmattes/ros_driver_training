// Copyright (c) 2015, Rich Mattes
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <chrono>
#include <csignal>
#include <cstdint>
#include <iostream>
#include <memory>
#include <thread>

#include <flexiport/flexiport.h>
#include <flexiport/port.h>

struct accel_message {
    uint16_t seq;
    int16_t x_accel;
    int16_t y_accel;
    int16_t z_accel;
    int32_t roll_rate;
    int32_t pitch_rate;
    int32_t yaw_rate;
}__attribute__((packed));

bool g_exit = false;

void int_signal_handler(int signal)
{
    g_exit = true;
}

int main (int argc, char *argv[])
{
    std::signal(SIGINT, int_signal_handler);
    std::unique_ptr<flexiport::Port> port(flexiport::CreatePort("type=udp dest_ip=127.0.0.1 dest_port=6300"));

    uint16_t seq = 0;
    try {
        port->Open();
        std::cout << "Writing messages..." << std::endl;
        while(!g_exit) {
            // Create and fill the message
            accel_message msg;
            msg.seq = seq++;
            msg.x_accel = 10 * 50;
            msg.y_accel = 100 * 50;
            msg.z_accel = 90 * 50;
            msg.roll_rate= 13 * 20000;
            msg.pitch_rate = 39 * 20000;
            msg.yaw_rate = 495 * 20000;

            port->Write(reinterpret_cast<char*>(&msg), sizeof(msg));
            std::cout << "Wrote Message " << msg.seq << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
    }
    catch (std::exception& e) {
        std::cerr << "Caught Exception: " << e.what() << std::endl;
    }

    std::cout << "Exiting." << std::endl;
    return 0;
}
