include_directories(${Flexiport_INCLUDE_DIRS})

# Lesson 1: Simple UDP String Publisher
add_executable(rdt_udp_string udp_string_device.cpp)
target_link_libraries(rdt_udp_string ${Flexiport_LIBRARIES})

# Lesson 2: Binary UDP Message Publisher
add_executable(rdt_udp_binary udp_binary_device.cpp)
target_link_libraries(rdt_udp_binary ${Flexiport_LIBRARIES})

install(TARGETS rdt_udp_string rdt_udp_binary DESTINATION ${CMAKE_INSTALL_BINDIR})
