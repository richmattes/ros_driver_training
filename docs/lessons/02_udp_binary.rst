=========================
Lesson 2: UDP Binary Data
=========================

Overview
========
In this lesson, you will:

* Make a stand-alone class to establish and read from a UDP socket
* Read binary values out of datagrams on the UDP socket
* Scale binary values to real-world units
* Publish the converted values to a ROS `sensor_msgs/Imu <http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html>`_ topic

Device Information
==================
The device in this lesson simulates an Inertial Measurement Unit (IMU,) which is a common sensor type in robots and other unmanned vehicles.
This IMU publishes linear accellerations and angular rates via UDP at a fixed rate, using the custom binary protocol described below.
Each UDP packet contains one sensor reading.

Interface
---------
This device publishes data over a UDP socket.
The data is published to the localhost address (127.0.0.1), over port 6300.

Protocol
--------
The simulated accelerometer sensor publishes a fixed-length binary message in each UDP datagram.
The table below describes the binary format for each message.

+------------+-----------+------------+------------+
| Field Name | Data Type | Data Count | Data Scale |
+============+===========+============+============+
| Sequence   | uint16    | 1          | 1          |
+------------+-----------+------------+------------+
| X Accel    | int16     | 1          | 50         |
+------------+-----------+------------+------------+
| Y Accel    | int16     | 1          | 50         |
+------------+-----------+------------+------------+
| Z Accel    | int16     | 1          | 50         |
+------------+-----------+------------+------------+
| Roll Rate  | int32     | 1          | 20,000     |
+------------+-----------+------------+------------+
| Pitch Rate | int32     | 1          | 20,000     |
+------------+-----------+------------+------------+
| Yaw Rate   | int32     | 1          | 20,000     |
+------------+-----------+------------+------------+

All of the integers published by the sensor are in little-endian byte order.
Intel PCs use little-endian byte order, so byte order conversion is not necessary for accurate parsing on intel platforms.
If you are using a big-endian architecture, you will need to byteswap the fields in the sensor data before scaling them.

The scale factors indicate that the values coming from the sensor are scaled by that value before being transmitted.
This is common in some sensor types, like in sensors that are sampling analog voltages from an IC and sending raw analog to digital sensor readings back to the user.
In these cases, there may be a conversion factor or function to convert from raw sensor readings to the desired physical units.

In this case, the simulated accelerometer is sampling analog voltages in all 6 dimensions using a 16 bit analog to digital converter.
Each count of the analog to digital converter corresponds to 1/50 of a meter/second^2, or 1/20,000 of a radian/second, respectively.
The driver that talks to this sensor must both read the values, correct the byte order, scale them to usable real-world units, and pack them into a ROS message.

Running the Device
------------------
Similar to the previous lesson, you can use the ``rosrun`` command to run the simulated device.
With your environment set up properly, you can run the following command:

.. code-block:: bash

   $ rosrun ros_driver_training rdt_udp_binary

You should see a message that the device is writing output.
Use CTRL+C to stop the program.

Writing the Device Interface (C++)
==================================
This section walks through the process of writing a driver node in C++ using the ROS interfaces.
It builds on the skills learned in Lesson 1, and adds some additional design considerations.

Design Considerations
---------------------
In Lesson 1, one big main() function contained all of the code in the ROS node.
This monolithic design worked for the simple device in Lesson 1, but it is often the case that the code for interacting with a device is too large to reasonably fit into a single function.
In these cases, the code to interface with the underlying device is better suited to be a stand-alone module.
Building the sensor interface in a separate module allows for testing the code for communication with the sensor outside of ROS, and for re-using the interface code for other frameworks if desired.

In this lesson, we will create a C++ class that interfaces with the hardware, and use that class from within a ROS node to gather data from the underlying device.
The class should minimally contain functions to:

* Open communication with the device (e.g. establish a UDP socket to read data from)
* Close communication with the device (e.g. shut down the UDP socket and clean up any resources)
* Read a sensor data packet from the device

In addition to creating a class to communicate with the deivce, we will also create an interface definition that describes the data being read from the device.

Define the sensor interface
---------------------------
The sensor in this example publishes data in the format described in the Protocol section.
To make reading and parsing the data easier, it is useful to create a structure that maps directly to the expected sensor output. 

Note: This method only works if the byte order (little-endian, big endian) of the sensor matches the byte order of the host PC running the parsing code.  If the byte ordering doesn't match, the message shouldbe adjusted after being read to restore it to the correct byte order.

Start by creating a file called "AccelInterface.h" to hold the sensor interface definition.  

.. code-block:: bash

   $ mkdir -p ~/ros_driver_workspace/src/driver_lessons/src/lesson_02
   $ touch ~/ros_driver_workspace/src/driver_lessons/src/lesson_02/AccelInterface.h


In this header, define a struct called "RawAccelMessage", as follows:

.. literalinclude:: lesson_02/AccelMessage.h 
   :language: c++
   :lines: 10-19

Additionally, define a struct called "AccelMessage", as follows:

.. literalinclude:: lesson_02/AccelMessage.h
   :language: c++
   :lines: 21-28

Finally, we can create a helper function to convert between the raw and processed messages, using the conversion factors described in the interface specification.  First, we will create constants to store the conversion factors.  Then, we define a function that takes a RawAccelMessage as its argument, returns an AccelMessage, and performs the necessary conversions and data type shifts.  The resulting file looks like:

.. literalinclude:: lesson_02/AccelMessage.h
      :language: c++


Create the Interface Class
--------------------------
Begin by creating a file called "AccelInterface.h" to hold the class definition for our accelerometer interface class.
Create a header file, ``AccelInterface.h`` and a source file ``AccelInterface.cpp`` to hold the class definition and implementation.

.. code-block:: bash

   $ cd ~/ros-driver-workspace/src/driver_lessons/src/lesson_02
   $ touch AccelInterface.h AccelInterface.cpp

Inside the class, we can define the following methods and properties:

* A constructor, which takes arguments that specify how to connect to the sensor device
* A method to read a data message from the underlying device

The resulting class definition should look like this:

.. literalinclude:: lesson_02/AccelInterface.h
   :language: c++
   :lines: 1-17,20-22

In addition to the class member functions, the class should also contain properties that include internal state, such as the UDP socket to read data from the sensor device.  As in lesson 1, this lesson will use the C sockets libraries to establish the UDP socket.

With the relevant properties added, the class definition should look like:

.. literalinclude:: lesson_02/AccelInterface.h
   :language: c++

Create an additional source file, ``AccelInterface.cpp``, the definitions for each of the member functions declared in the class interface:

.. literalinclude:: lesson_02/AccelInterfaceSkeleton.cpp
       :language: c++

The following sections will walk through filling in these functions.

Connect to the sensor
---------------------
With the class defined, we can begin writing the class member functions.  The first function to write is the ``SetupSocket`` member function, which opens a UDP socket to read from the sensor.  Begin by establishing the class member ``udp_socket_`` with the ``socket()`` call:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 15-22,40

After that, we need to convert the IP address string to a format that the socket library can use:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 15-28,40
   :emphasize-lines: 9-13 

Next, we need to create a structure that describes the address and port to listen to for UDP messages:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 15-35,40
   :emphasize-lines: 15-20

Next, we set a timeout value for calls to ``recv``.
Without a timeout value, a call to ``recv`` can block indefinitely if no data is being sent to the UDP port.

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 15-40
   :emphasize-lines: 22-26

Finally, we bind the UDP socket to the desired address and port.  The completed function looks like:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 15-46
   :emphasize-lines: 28-31

Read and parse data from the sensor
-----------------------------------
Now that a connection has been established to the sensor, we need a function to read the data from the sensor, parse it, and return it in a usable format.  In this section, we will accomplish those goals by filling the ``ReadMessage`` member function of the ``AccelInterface`` class.

Begin by reading from the UDP socket using the  ``recv`` function:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 48-52,58-59

The ``recv`` function returns the number of bytes read from the UDP socket, which corresponds to the number of bytes in the UDP datagram that has been received by the network interface.  We expect that each time we call ``recv`` on the socket, we should read the exact amount of bytes in each sensor datagram, equal to the size of the structure we declared based on the sensor protocol definition.  If the correct number of bytes are read, we can copy the bytes directly in to the raw data structure, otherwise we should ignore the received datagram.

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 48-54, 57-59
   :emphasize-lines: 6-8

If the raw message size is correct, we can use the helper function we created earlier to convert and scale the raw message to the correct format.  We then return "true", indicating that a message was successfully read.

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++
   :lines: 48-59
   :emphasize-lines: 8-9

Tying the class together
------------------------
After the above sections, you should have a class that looks like the following:

.. literalinclude:: lesson_02/AccelInterface.cpp
   :language: c++

Build the AccelInterface class
------------------------------
Now that you've created the AccelInterface class, you can build the class into a C++ library to be used by other programs (ROS nodes, third party programs, etc.)

Build the library by adding the following to the end of the ``CMakeLists.txt`` file in the driver_lessons directory:

.. code-block:: cmake
   
   add_library(AccelInterface STATIC src/lesson_02/AccelInterface.cpp)

Write a test program to use the class
-------------------------------------
Test programs are useful to test your class without the need for the whole ROS framework.
The sensor interface class is self-contained, so it is easy to create a small test driver program to run the accelerometer interfac class code and print output screen.

Create a file called "accel_sensor_test.cpp" in the lesson_02 directory:

.. code-block:: bash

   mkdir -p ~/ros_driver_workspace/src/driver_lessons/src/lesson_02/accel_sensor_test.cpp

Add the following main() function to the file:

.. code-block:: c++

   int main(int argc, char **argv)
   {
        return 0;
   }

To build the file, you need to add a CMake command to create an executable from the file.
Since we plan to use the ``AccelInterface`` from this library, we also need to add a CMake command to link your executable to the ``AccelInterface`` library we created earlier.
Add the following lines to the CMakeLists.txt file in the driver_lessons subdirectory to build your test program and link to the ``AccelInterface`` library:

.. code-block:: cmake

   add_executable(accel_sensor_test src/lesson_02/accel_sensor_test.cpp)
   target_link_libraries(accel_sensor_test AccelInterface)

Now back to the code.
To use the accelerometer class in the ``accel_sensor_test.cpp`` file, we need to include the AccelInterface class definition, and instantiate an instance of the class in our main function, as follows:

.. code-block:: c++

   #include "AccelInterface.h"
   int main (int argc, char **argv)
   {
       AccelInterface accel("127.0.0.1", 6300);

       return 0;
   }

From there, we can create an infinite loop that tries to read values from the accelerometer and prints them to the screen:

.. code-block:: c++

   #include "AccelInterface.h"
   #include <iostream>
   int main (int argc, char **argv)
   {
       AccelInterface accel("127.0.0.1", 6300);
       AccelMessage message;
       while(1) {
           if (accel.ReadMessage(message)) {
               std::cout << "Read message with Sequence: " << message.seq << std::endl;
           }
           else {
               std::cout << "Failed to read message" << std::endl;
           }
       }
       return 0;
   }

Run the test program
--------------------
The rosrun command can be used to run the simulated accelerometer device as well as the test program we just wrote.

In one terminal window, use rosrun to execute the ``rdt_udp_binary`` program:

.. code-block:: bash

   $ rosrun ros_driver_training rdt_udp_binary

In a second terminal window, execute your test program with rosrun:

.. code-block:: bash

   $ rosrun driver_lessons accel_sensor_test

You should see your test program printing messages that indicate it is reading message sequence numbers.
Press CTRL+C in both terminal windows to exit these programs.

Writing the Node (C++)
======================
The ROS node to publish the acceleromter can be structured to the test program written in the earlier section.
The ROS node will spin in an infinte loop reading messages from the sensor, fill the values read from the sensor into a ROS message, and publish that ROS mesage over a ROS topic.

Add the file to the driver package
----------------------------------
Similar to lesson 1, we will create a file named ``udp_binary_node.cpp`` in the driver package.
This file will contain the ROS node that interacts with the interface we just built.

.. code-block:: bash
   
   $ touch ~/ros-driver-workspace/src/driver_lessons/src/lesson_02/udp_binary_node.cpp

You will also need to add a CMake command to build the driver node, and link the node to the catkin libraries (required for the ROS C++ runtime and message definitions) and the AccelInterface library we created.
Add the following lines to the end of CMakeLists.txt in the driver_lessons folder:

.. code-block:: cmake

   add_executable(udp_binary_node src/lesson_02/udp_binary_node.cpp)
   target_link_libraries(udp_binary_node ${catkin_LIBRARIES} AccelInterface)

Write the driver node
---------------------
Start with a bare driver node:

.. literalinclude:: lesson_02/udp_binary_node.cpp.1
   :language: c++

Then include the ``AccelInterface.h`` header, and create an ``AccelInterface`` object.
The ``AccelInterface`` object is initialized with the ip address and port specified in the Device Information section.

.. literalinclude:: lesson_02/udp_binary_node.cpp.2
   :language: c++
   :emphasize-lines: 5,17-18

Add a call to ``AccelInterface::ReadMessage()`` to the main loop, and check its return value.
If the return value is ``true``, we know that the library read a message from the sensor, so we can copy the information in the ROS message structure and publish a ROS message.
We also set the ROS message header's time stamp to the current system time, to roughly indicate the time at which the sensor reading was made.
If the return value is ``false``, then the library timed out trying to read a new message, so we let the driver loop and try again.

.. literalinclude:: lesson_02/udp_binary_node.cpp.3
   :language: c++
   :emphasize-lines: 23-34

Run the driver node
-------------------
Build the driver using ``catkin_make``, and then use rosrun to run the device simulator:

.. code-block:: bash

   $ rosrun ros_driver_training rdt_udp_binary

In another terminal window, run roscore and the driver program:

.. code-block:: bash

   $ roscore&
   $ rosrun driver_lessons udp_binary_node

You should see INFO messages from your driver node on the console once per second indicating that the driver has published a message.

To view the contents of these messages, use rostopic in a new terminal window:

.. code-block:: bash
   
   $ rostopic echo /imu

You should see Imu messages being published once a second, with incrementing timestamps in the header and constant non-zero values in the "linear_acceleration" and "angular_velocity" fields.

Use CTRL+C to close the programs started in the different terminal windows.
