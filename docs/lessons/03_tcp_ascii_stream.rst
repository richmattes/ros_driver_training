===========================
Lesson 3: TCP ASCII Streams
===========================

Overview
========
In this lesson, you will:

* Establish and read from a TCP socket
* Read ASCII messages from a TCP stream
* Validate ASCII messages read from the TCP stream
* Publish messages to the following ROS topics
  * 
* Handle reconnecting when the device disconnects

Device Information
==================
The device in this lesson simulates a power monitoring sensor, another common type of sensor in robotics and other unmanned vehicles.
This particular sensor measures the voltage and current draw for up to 8 different power channels
It also periodically reports its own status, including the device's serial number, temperature, etc.

Interface
---------
This device publishes data over a TCP socket
The data is published to the localhost address (127.0.0.1), over port 4200.

Protocol
--------
The simulated sensor publishes several ASCII messages, at various rates.
These messages follow the format:

``%XXXXX,xx.xxx,xx.xxx,xx.xxx,xx.xxx,xx.xxx,xx.xxx,xx.xxx,xx.xxx^SS<CR><LF>``

* Each message begins with the % symbol
* The name field indicates the name of the message type
* The value fields contain values that are prescribed by the message type
* All name and value fields are comma-separated
* The ^ character indicates the end of the values, and the beginning of the message checksum
* The message ends with the ASCII Carriage Return <CR> and Line Feed <LF> characters

There are four different message types that the sensor can publish:
Current: Current draw of up to 8 channels, in amperes
Voltage: Voltage potential of up to 8 channels, in volts.
Status: Device serial number, device temperature

The checksum is the sum of all of the bytes after the % sign and before the carat sign, truncated to 8 bits.
It is represented by two hexadecimal digits.

Running the Device
------------------
You can use ``rosrun`` to run the simulated device.
With your environment set up properly, run the following command:

.. code-block:: bash

   $ rosrun ros_driver_training rdt_tcp_ascii_stream

You should see messages indicating that the device is writing output.
Use CTRL+C to stop the program.
