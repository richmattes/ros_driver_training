#ifndef ACCEL_MESSAGE_H
#define ACCEL_MESSAGE_H

#include <cstdint>

//! Conversion terms from sensor protocol definition
const float ACCEL_CONVERSION_FACTOR = 50.0F;
const float GYRO_CONVERSION_FACTOR = 20000.0F;

/*! \brief Raw Accelerometer Message
 * Raw, unscaled accelerometer message read directly from sensor.
 * Packed attribute is used to make sure that the compiler does not
 * add any extra padding between the data fields.
 */
struct RawAccelMessage {
    uint16_t sequence; //!< Message sequence number
    int16_t accel[3];  //!< (x,y,z) acceleration, sensor units
    int32_t gyro[3];   //!< (roll,pitch,yaw) acceleration, sensor units
}__attribute__((packed));

/*! \brief Accelerometer Message
 * Accelerometer message definition in real-world units.
 */
struct AccelMessage {
    uint16_t sequence; //!< Message sequence number
    float accel[3];    //!< (x,y,z) acceleration, meters/second^2
    float gyro[3];     //!< (roll,pitch,yaw) acceleration, radians/second^2
};

/*! \brief Convert and Scale Raw Accel Message
 * This function converts a raw accelerometer structure to a scaled
 * accelerometer message, using the conversion factors from the
 * sensor protocol definition.
 * \param input Raw accelerometer message to convert
 * \return Scaled and coverted accelerometer message
 */
static AccelMessage convertRawMessage(const RawAccelMessage& input) {
    AccelMessage output;
    output.sequence = input.sequence;
    for (int i = 0; i < 3; ++i) {
        output.accel[i] = input.accel[i] / ACCEL_CONVERSION_FACTOR;
        output.gyro[i] = input.gyro[i] / GYRO_CONVERSION_FACTOR;
    }
    return output;
}

#endif // ACCEL_MESSAGE_H
