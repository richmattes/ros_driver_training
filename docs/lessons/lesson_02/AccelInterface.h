#ifndef ACCEL_INTERFACE_H
#define ACCEL_INERRFACE_H

#include "AccelMessage.h"

#include <cstdint>
#include <string>

class AccelInterface {
public:
    AccelInterface(std::string address, uint16_t port);

    bool ReadMessage(AccelMessage &message);

private:
    void SetupSocket(std::string address, uint16_t port);
    
    int32_t udp_socket_;
    uint8_t read_buffer_[512];
};

#endif
