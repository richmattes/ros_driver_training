#include "AccelInterface.h"

#include <iostream>     // for cout, cerr
#include <sys/socket.h> // for socket()
#include <netinet/in.h> // for in_addr()
#include <arpa/inet.h>  // for inet_aton()
#include <string.h>     // for memcpy()

AccelInterface::AccelInterface(std::string address, uint16_t port) 
{
    udp_socket_ = 0;
    SetupSocket(address, port);
}

void AccelInterface::SetupSocket(std::string address, uint16_t port)
{
    udp_socket_ = socket(AF_INET, SOCK_DGRAM, 0);
    if (udp_socket_ < 0) {
        std::cerr << "Error opening socket" << std::endl;
        return;
    }
    
    // Convert the IP address string to its binary representation
    in_addr listen_ip;
    if (inet_aton(address.c_str(), &listen_ip) < 0) {
        std::cerr << "Invalid IP address " << address << std::endl;
    }

    // Create the structure that tells the socket where to listen for UDP messages
    sockaddr_in listen_addr;
    memset(&listen_addr, 0, sizeof(sockaddr_in));   // Clear out the structure
    listen_addr.sin_family = AF_INET;               // Set the protocol family
    listen_addr.sin_addr.s_addr = listen_ip.s_addr; // Set the IP to listen to
    listen_addr.sin_port = htons(port);             // Set the port to listen to

    // Set a 100ms timeout for the socket so calls to recv() don't block indefinitely
    timeval timeout = {0, 100000};
    if (setsockopt(udp_socket_, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
        std::cerr << "Could not set UDP socket timeout" << std::endl;
    }

    // Bind the socket to our listen address to start receiving UDP messages
    if (bind(udp_socket_, (sockaddr*)&listen_addr, sizeof(listen_addr)) < 0) {
        std::cerr << "Could not bind to UDP socket" << std::endl;
    }
}

bool AccelInterface::ReadMessage(AccelMessage &message)
{
    RawAccelMessage raw_message;

    int bytes_read = recv(udp_socket_, &read_buffer_, sizeof(read_buffer_), 0);
    if (bytes_read == sizeof(raw_message)) {
        memcpy(&raw_message, read_buffer_, sizeof(raw_message));
        message = convertRawMessage(raw_message);
        return true;
    }
    return false;
}

