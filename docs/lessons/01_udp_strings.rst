=====================
Lesson 1: UDP Strings
=====================

Overview
========
In this lesson, you will:

* Establish and read from a UDP socket
* Read string out of datagrams on the UDP socket
* Publish the strings to a ROS `std_msgs/String <http://docs.ros.org/api/std_msgs/html/msg/String.html>`_ topic

Device Information
==================
The device that we're writing a driver to interface to is a networked device publishing a single ASCII string at a fixed rate. This type of device is a little contrived, but it provides a good introduction to building a ROS driver node.

Interface
---------
This device publishes data over UDP. The data is published to the localhost address (127.0.0.1), over port 4000.

Protocol
--------
The simulated device in the lesson publishes a fixed-length null-terminated ASCII string in each UDP datagram.  The table below describes the binary format.

+------------+-----------+------------+
| Field Name | Data Type | Data Count |
+============+===========+============+
| Text       | char      | 13         |
+------------+-----------+------------+

The device publishes its data message at a rate of 1 time per second.

Running the Device
------------------
The simulated device for this lesson is called "rdt_udp_string".
You can run it using the following command:

.. code-block:: bash

   # Make sure your workspace is on the ROS package search path
   $ source ~/ros_driver_workspace/deve/setup.sh
   # Start the program using rosrun
   $ rosrun ros_driver_training rdt_udp_string

After you run the program, you should see feedback every second that it's working:

.. code-block:: bash

   Writing Message: Hello, World!
   Writing Message: Hello, World!
   Writing Message: Hello, World!
   Writing Message: Hello, World!
   ...

Press CTRL+C to stop the program.

Writing the Node (C++)
======================
This section walks through the process of writing a driver node program in C++ using the ROS interfaces.

Add the file to the driver package
----------------------------------
Create a file named ``udp_string_node.cpp`` in your driver package.  You will use this file to write the code to complete this lesson.

.. code-block:: bash

   $ mkdir -p ~/ros_driver_workspace/src/driver_lessons/src/lesson_01
   $ cd ~/ros_driver_workspace/src/driver_lessons/src/lesson_01
   $ touch udp_string_node.cpp

You also need to add the source file to your catkin project's buildsystem so that it builds as an executable node program.  Add the following lines to the CMakeLists.txt file at the root of the driver_lessons package, at the end of the section marked "Build":

.. code-block:: cmake
   
   add_executable(udp_string_node src/lesson_01/udp_string_node.cpp)
   target_link_libraries(udp_string_node ${catkin_LIBRARIES})

Creating the node
-----------------
To start, we will first fill in a basic node like the `Simple Publisher <http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29>`_ found in the ROS tutorials.  This node publishes a blank string message 10 times each second to a ``std_msgs::String`` topic.  It does not yet read data from our UDP device, but it does set up the framework for publishing data to a ROS topic once we start reading from the UDP device.

Add the following code to your udp_string_driver.cpp:

.. literalinclude:: lesson_01/udp_string_node.cpp.1
   :language: c++

Reading from the device
-----------------------
Now that we have a basic node framework, we need to write the code to read data from the device, parse the data, and publish it to our ``std_msgs::String`` topic.

First, we need to open a UDP socket to read from the device.  While many third-party libaries have functionality to read from UDP sockets like boost_asio, Flexiport, and Qt, we will opt to use the standard C socket interface for this tutorial.  Feel free to experiment with other libraries as you see fit.

The highlighted sections in the code below handle setting up and opening a UDP socket with at the IP address and port that the device communicates on.

.. literalinclude:: lesson_01/udp_string_node.cpp.2
   :language: c++
   :emphasize-lines: 5-7,11-39,62

Reading from the device
-----------------------
Now that we have opened the connection to the device, we need to write code to read from the device and publish the data that we read from the device to the ``string`` topic.  To do so, we need to accomplish the following:

* Identify when data is ready at our UDP socket
* Read the data from the UDP socket
* Format it into a std_msgs::String structure
* Publish the string

First, we need to identify when data is ready from the UDP socket.  There are two ways to do this, either by using a system call like ``select()`` in order to tell us when data is ready to be read from the socket.  The easier way is to just try to read from the socket whether or not you know data is there.  If there is no data avilable yet, the call to the ``recv()`` function returns -1 and sets the errno value to indicate what went wrong.  

We know from the device specifications that we expect data to arrive at the UDP socket once a second, so we need to check for data at the UDP socket at least that quickly or risk falling behind.  Our node's main loop is already set to execute 10 times per second via the ``loop_rate`` object, which is sufficiently fast to collect data arriving once per second.  

Given all that, we will attempt to read from the UDP socket once every iteration of the node's main loop.  We know that the call to recv will return -1 if no data is available, so in that case we should check errno to make sure that there's no data available (errno will also indicate if there is an error in the socket call).  If there was an unexpected error, the node should report it, and if the error was expected the node should continue on for another iteration.  

If the call to ``recv`` does return a positive value, it means that we've read the data from the driver.  That data should then be parsed and re-packaged into a std_msgs::String, and published.

The added code to accomplish this is shown below.

.. literalinclude:: lesson_01/udp_string_node.cpp.3
   :language: c++
   :emphasize-lines: 49-51,55-84

Testing your driver
-------------------
Now that the driver is written, it's time to test it and make sure it works properly.  First, we need to build the driver using the ``catkin_make`` command

.. code-block:: bash

   $ cd ~/ros_driver_workspace
   $ catkin_make

Once the code is built, we need to add our current catkin workspace to our ``ROS_PACKAGE_PATH`` environment variable.  The easiest way to do this is to ``source`` the ``setup.bash`` generated by catkin in the workspace.

.. code-block:: bash

   $ source devel/setup.bash

Now we can use ``rosrun`` to run the driver node

.. code-block:: bash

   $ roscore &
   $ rosrun driver_lessons udp_string_node 

Finally, in a new terminal window, we need to run the program that simulates the device, as described in the Device Information section.  

Once all of these are running you should see INFO messages like the following:

.. code-block:: bash

   [ INFO] [4.162325866]: Read 13 bytes from the socket
   [ INFO] [4.162405798]: Publishing message
   [ INFO] [5.162337379]: Read 13 bytes from the socket
   [ INFO] [5.162416940]: Publishing message

In a new terminal, echo the data from the device using ``rostopic``

.. code-block:: bash
   
   $ source ~/ros_driver_workspace/devel/setup.sh
   $ rostopic echo /string

You should see output like the following:

.. code-block:: bash

   data: Hello, World!
   ---
   data: Hello, World!
   ---
   data: Hello, World!
   ---

Congratulations!  You've just written a driver that reads a string from a UDP device and publishes it to a ROS node!

Use CTRL+C to stop the programs you launched using rosrun.
