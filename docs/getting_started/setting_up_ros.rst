Setting Up ROS
--------------

Before you can practice writing driver nodes, you need to install ROS on your system.  Follow the `installation instructions <http://wiki.ros.org/ROS/Installation>`_ on the ROS wiki for your system.

Once you have completed the installation, you can continue to the next section.
