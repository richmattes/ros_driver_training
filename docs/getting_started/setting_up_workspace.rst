Setting up the ROS workspace
============================
In order to develop ROS driver nodes, you must first create a ROS workspace so that you can compile and run your nodes with the ROS catkin build tool.

Set up a catkin workspace
-------------------------
ROS nodes and related code are built within catkin workspaces.
To create a catkin workspace, first create a directory for catkin projects.
For the ROS driver lessons, we will create a workspace called ``ros_driver_workspace`` in the current user's home directory.
This workspace will hold the simulated devices provided by this ROS driver training program, as well as the ROS nodes you will write to interact with the simulated devices.
See the ROS `Workspace Creation Tutorial <http://wiki.ros.org/catkin/Tutorials/create_a_workspace>_` for more detail

.. code-block:: bash

   # Create the ros_driver_workspace directory in your home directory
   $ mkdir -p ~/ros_driver_workspace/src
   $ cd ~/ros_driver_workspace/src
   $ catkin_init_workspace

Download the ROS Driver Training code
-------------------------------------
Once you've created your catkin workspace, you can download the ROS driver training code.
This repository contains the code to create the simulated devices that you will program your ROS driver nodes for, as well as the source for this documentation.
Download the 

Creating a new catkin package
-----------------------------
To build ROS drivers, you must first create a new ROS package using the catkin tool.  The instructions here are based off of the ROS `Package Creation Tutorial <wiki.ros.org/ROS/Tutorials/CreatingPackage>`_.

.. code-block:: bash

   # This directory should already exist from the "Installation" section
   $ mkdir -p ~/ros-driver-workspace/src
   $ cd ~/ros-driver-workspace/src
   # Make sure you have sourced the ROS setup.sh file
   $ catkin_create_pkg driver_lessons std_msgs sensor_msgs rospy roscpp

Building the catkin package
---------------------------
Test that the catkin package builds successfully:

.. code-block:: bash
   $ cd ~/ros-driver-workspace
   $ catkin_make

