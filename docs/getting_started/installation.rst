Install the ROS Driver Training code
====================================

System Requirements
-------------------
The ROS Driver Traning package requires several libraries and features for build and installation.

* A C++ compiler with C++11 support
* CMake 2.8.12 or higher (`Project Site <http://cmake.org>`_)
* Flexiport 2.0.0 or higher (`Project Site <http://gbiggs.github.io/flexiport/>`_)
* ROS (`Project Site <http://ros.org>`_)
* git (`Project Site <http://git-scm.com>`_)

Download and Build
------------------
The ROS Driver Training package is a catkin package, it can be built in the same catkin worksapce that you will use to write the ROS nodes for this tutorial.
Clone the project's git repository from Bitbucket into your ros_driver_workspace catkin workspace:

.. code-block:: bash

   $ cd ~/ros_driver_workspace/src
   $ git clone https://bitbucket.org/rmattes/ros_driver_training.git

Then compile the ROS driver training code using catkin.
Catkin will build the newly downloaded ros_driver_training code, as well as the driver_lessons package you created in the previous step.

.. code-block:: bash

   $ cd ~/ros_driver_workspace
   $ catkin_make

Test the Installation
---------------------
At this point, you should be able to run the simulated devices from ROS Driver Training package.
First source the setup.sh file created for the ros_driver_workspace workspace, then use rosrun to launch one of the example devices.

.. code-block:: bash

   $ source ~/ros_driver_workspace/deve/setup.sh
   $ rosrun ros_driver_training rdt_udp_string

You should see output that the simulated device is writting "Hello, World!"
Press CTRL+C to stop the program, and move on to the next section.
