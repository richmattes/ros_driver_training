Getting Started
===============

.. toctree::
   :maxdepth: 2

   setting_up_ros
   setting_up_workspace
   installation
