.. include:: ../README.rst

Contents
========

.. toctree::
   :maxdepth: 2

   getting_started/getting_started
   lessons/lessons

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

